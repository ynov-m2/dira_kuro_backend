import logging

from flask import request, jsonify, abort, Blueprint
from common.server.flaskutils import print_flush
from common.db.tools import serialize_foreign_recursive, to_dict
from common.db.base import Database
from common.utils.security import authentication_required, generate_user_token, permissions
from db.models import Account
from server import *
from config.settings import SETTINGS
from utils.permissions import Permissions

# This blu print need tobe load by server/app.py in the register_blueprints function
blueprint = Blueprint('views', __name__)

LOG = logging.getLogger(__name__)


# blueprint.add_resource(lines.LineListView, '/api/lines')

@blueprint.route('/status/is_up', methods=['GET'])
def is_up():
    return jsonify(is_up=True)

@blueprint.route('/status/admin', methods=['GET'])
@authentication_required
@permissions(permissions=(Permissions.ADMIN))
def test_admin_permissions(user):
    return jsonify(is_up=False)

@blueprint.route('/api/users/self', methods=['GET'])
@authentication_required
def get_my_user_info(user):
    return jsonify(user=user)



@blueprint.route('/api/users/', methods=['POST'])
# @authentication_required
# @permissions(permissions=(Permissions.ADMIN))
def create_user():
    permission = request.get_json().get('permission', False)
    username = request.get_json().get('username', False)
    password = request.get_json().get('password', False)
    email = request.get_json().get('email', False)

    if not username or not password:
        abort(405)
    
    with Database(auto_commit=True) as db:
        account = db.query(Account).filter_by(name=username).first()
        if not account:
            account = Account(name=username)
            db.add(account)
        if account.password:
            abort(409)

        account.set_password(password)
        account.permission = permission
        account.email = email

        return jsonify(username, account.password, account.email)

@blueprint.route('/api/users/login', methods=['POST'])
def user_login():
    username = request.get_json().get('username', False)
    password = request.get_json().get('password', False)
    account = False
    
    with Database(auto_commit=True) as db:
        account = db.query(Account).filter_by(name=username).first()
        if not account:
            abort(404)
        if not account.check_password(password):
            abort(403)

        return jsonify(login=True, token=generate_user_token(account.get_data()).decode('utf8'))